/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper;

import java.util.function.Consumer;

import org.bonsaimind.jluascript.lua.LuaEnvironment;
import org.bonsaimind.jlwebscraper.lua.JsonToLuaFunction;
import org.bonsaimind.jlwebscraper.web.communication.Cookie;
import org.bonsaimind.jlwebscraper.web.communication.CookieJar;
import org.bonsaimind.jlwebscraper.web.communication.Header;
import org.bonsaimind.jlwebscraper.web.communication.Headers;
import org.bonsaimind.jlwebscraper.web.communication.HttpStatus;
import org.bonsaimind.jlwebscraper.web.communication.HttpStatusType;
import org.bonsaimind.jlwebscraper.web.communication.PostData;
import org.bonsaimind.jlwebscraper.web.communication.PostDataItem;
import org.bonsaimind.jlwebscraper.web.communication.RedirectBehavior;
import org.bonsaimind.jlwebscraper.web.communication.Request;
import org.bonsaimind.jlwebscraper.web.html.Document;

/**
 * The {@link WebScraperLuaEnvironment} is the main environment for running
 * scripts.
 */
public class WebScraperLuaEnvironment extends LuaEnvironment {
	/**
	 * Creates a new instance of {@link WebScraperLuaEnvironment}.
	 */
	public WebScraperLuaEnvironment() {
		super();
		
		addDefaults();
	}
	
	/**
	 * Adds the default imports and classes.
	 */
	protected void addDefaults() {
		importClass(Cookie.class);
		importClass(CookieJar.class);
		importClass(Header.class);
		importClass(Headers.class);
		importClass(HttpStatus.class);
		importClass(HttpStatusType.class);
		importClass(PostData.class);
		importClass(PostDataItem.class);
		importClass(RedirectBehavior.class);
		importClass(Request.class);
		
		importClass(Document.class);
		
		importClass(Consumer.class);
		
		environment.set("jsonToLua", new JsonToLuaFunction());
	}
}
