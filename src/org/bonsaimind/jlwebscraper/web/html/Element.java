/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.html;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Consumer;

/**
 * The {@link Element} represents a single HTML element/tag and its content.
 */
public class Element {
	/** The resource {@link org.jsoup.nodes.Element}. */
	protected org.jsoup.nodes.Element element = null;
	/** The cache of elements. */
	protected Map<org.jsoup.nodes.Element, WeakReference<Element>> elementsCache = new WeakHashMap<>();
	
	/**
	 * Creates a new instance of {@link Element}.
	 *
	 * @param element The resource {@link org.jsoup.nodes.Element}.
	 */
	public Element(org.jsoup.nodes.Element element) {
		super();
		
		this.element = element;
	}
	
	/**
	 * Runs the given {@link Consumer} for every {@link Element} which matches
	 * the given (CSS) selector.
	 * 
	 * @param selector The (CSS) selector which the elements should match.
	 * @param action The action to perform for every matching element.
	 */
	public void foreach(String selector, Consumer<Element> action) {
		for (Element element : getAll(selector)) {
			action.accept(element);
		}
	}
	
	/**
	 * Gets all {@link Element}s matching the given (CSS) selector.
	 * 
	 * @param selector The (CSS) selector which the elements should match.
	 * @return A {@link List} of all matching {@link Element}s, an empty list of
	 *         non match.
	 */
	public List<Element> getAll(String selector) {
		List<Element> elements = new ArrayList<>();
		
		for (org.jsoup.nodes.Element element : element.select(selector)) {
			elements.add(cache(element));
		}
		
		return elements;
	}
	
	/**
	 * Gets the {@link String} value of the attribute with the given name.
	 * 
	 * @param name The name of the attribute, case insensitive.
	 * @return The {@link String} value of the attribute, an empty
	 *         {@link String} if the attribute does not exist.
	 * @see #hasAttribute(String)
	 */
	public String getAttribute(String name) {
		return element.attr(name);
	}
	
	/**
	 * Gets the first {@link Element} which matches the given (CSS) selector.
	 * 
	 * @param selector The (CSS) selector which the element should match.
	 * @return The first {@link Element} matching the given (CSS) selector,
	 *         {@code null} if there is none.
	 */
	public Element getFirst(String selector) {
		return cache(element.selectFirst(selector));
	}
	
	/**
	 * Gets the inner HTML {@link String} of this {@link Element}, meaning all
	 * children as {@link String}.
	 * 
	 * @return The inner HTML {@link String} of this {@link Element}, meaning
	 *         all children as {@link String}.
	 * @see #getOuterHtml()
	 */
	public String getInnerHtml() {
		return element.html();
	}
	
	/**
	 * Gets the outer HTML {@link String} of this {@link Element}, meaning this
	 * {@link Element} and all children as {@link String}.
	 * 
	 * @return The inner HTML {@link String} of this {@link Element}, meaning
	 *         this {@link Element} and all children as {@link String}.
	 * @see #getInnerHtml()
	 */
	public String getOuterHtml() {
		return element.outerHtml();
	}
	
	/**
	 * Gets the name of tag of this {@link Element}.
	 * 
	 * @return The name of tag of this {@link Element}.
	 */
	public String getTagName() {
		return element.tagName();
	}
	
	/**
	 * Gets all the text content of this {@link Element}.
	 * 
	 * @return All the text content of this {@link Element}.
	 */
	public String getText() {
		return element.text();
	}
	
	/**
	 * Gets if the attribute with the given name is set.
	 * 
	 * @param name The name of the attribute, case insensitive.
	 * @return {@code true} if the attribute exists.
	 */
	public boolean hasAttribute(String name) {
		return element.hasAttr(name);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return element.outerHtml();
	}
	
	/**
	 * Caches the given {@link org.jsoup.nodes.Element} and returns the wrapped
	 * and cached {@link Element}.
	 * 
	 * @param originalElement The {@link org.jsoup.nodes.Element} to wrap and
	 *        cache.
	 * @return The wrapped and cached {@link Element}.
	 */
	protected Element cache(org.jsoup.nodes.Element originalElement) {
		if (originalElement == null) {
			return null;
		}
		
		WeakReference<Element> reference = elementsCache.get(originalElement);
		
		if (reference != null) {
			Element cachedElement = reference.get();
			
			if (cachedElement != null) {
				return cachedElement;
			}
		}
		
		Element cachedElement = new Element(originalElement);
		
		elementsCache.put(originalElement, new WeakReference<>(cachedElement));
		
		return cachedElement;
	}
}
