/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.html;

import org.jsoup.parser.Parser;

/**
 * The {@link Document} represents a single HTML document.
 * 
 * @see Element
 */
public class Document extends Element {
	/** The resource {@link org.jsoup.nodes.Document}. */
	protected org.jsoup.nodes.Document document = null;
	
	/**
	 * Creates a new instance of {@link Document}.
	 *
	 * @param document The resource {@link org.jsoup.nodes.Document}.
	 */
	public Document(org.jsoup.nodes.Document document) {
		super(document);
		
		this.document = document;
	}
	
	/**
	 * Creates a new instance of {@link Document}.
	 *
	 * @param html The {@link String HTML string}.
	 */
	public Document(String html) {
		this(Parser.parse(html, ""));
	}
	
	/**
	 * Get the body {@link Element Element}.
	 * 
	 * @return The body {@link Element Element}.
	 */
	public Element getBody() {
		return cache(document.body());
	}
	
	/**
	 * Get the head {@link Element Element}.
	 * 
	 * @return The head {@link Element Element}.
	 */
	public Element getHead() {
		return cache(document.head());
	}
	
	/**
	 * Gets the location of the current document.
	 * 
	 * @return The location of the current document.
	 */
	public String getLocation() {
		return document.location();
	}
	
	/**
	 * Gets the title of the current document.
	 * 
	 * @return The title of the current document.
	 */
	public String getTitle() {
		return document.title();
	}
}
