/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class AbstractData<DATA_TYPE extends AbstractData<DATA_TYPE, ITEM_TYPE>, ITEM_TYPE extends AbstractKeyValuePair> {
	protected List<ITEM_TYPE> data = new ArrayList<>();
	protected Map<String, ITEM_TYPE> dataByKey = new LinkedHashMap<>();
	private Class<ITEM_TYPE> itemTypeClass = null;
	private List<ITEM_TYPE> readonlyData = null;
	
	protected AbstractData(Class<ITEM_TYPE> itemTypeClass) {
		super();
		
		this.itemTypeClass = itemTypeClass;
	}
	
	protected AbstractData(Class<ITEM_TYPE> itemTypeClass, DATA_TYPE source) {
		this(itemTypeClass);
		
		if (source != null) {
			for (ITEM_TYPE item : source.getAll()) {
				add(item);
			}
		}
	}
	
	protected AbstractData(Class<ITEM_TYPE> itemTypeClass, Map<String, String> source) {
		this(itemTypeClass);
		
		if (source != null) {
			for (Entry<String, String> entry : source.entrySet()) {
				add(entry.getKey(), entry.getValue());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public DATA_TYPE add(ITEM_TYPE item) {
		if (item == null) {
			return (DATA_TYPE)this;
		}
		
		data.remove(dataByKey.put(item.getKey(), item));
		data.add(item);
		
		return (DATA_TYPE)this;
	}
	
	public DATA_TYPE add(String key, String value) {
		try {
			return add(itemTypeClass.getConstructor(String.class, String.class).newInstance(key, value));
		} catch (ReflectiveOperationException | SecurityException e) {
			throw new RuntimeException("Failed to instantiate ITEM_TYPE.", e);
		}
	}
	
	public ITEM_TYPE get(String key) {
		return dataByKey.get(key);
	}
	
	public List<ITEM_TYPE> getAll() {
		if (readonlyData == null) {
			readonlyData = Collections.unmodifiableList(data);
		}
		
		return readonlyData;
	}
	
	public boolean has(String key) {
		return dataByKey.get(key) != null;
	}
	
	@Override
	public String toString() {
		StringBuilder stringRepresentation = new StringBuilder();
		
		for (ITEM_TYPE item : data) {
			stringRepresentation.append(item);
			stringRepresentation.append("\n");
		}
		
		return stringRepresentation.toString();
	}
}
