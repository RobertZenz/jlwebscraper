/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication;

import java.util.Map;

import org.bonsaimind.jlwebscraper.web.communication.base.AbstractData;

/**
 * A collection of {@link Header}s of a request or from a response.
 */
public class Headers extends AbstractData<Headers, Header> {
	/**
	 * Creates a new instance of {@link Headers}.
	 */
	public Headers() {
		super(Header.class);
	}
	
	/**
	 * Creates a new instance of {@link Headers}.
	 *
	 * @param source The {@link Headers source}.
	 */
	public Headers(Headers source) {
		super(Header.class, source);
	}
	
	/**
	 * Creates a new instance of {@link Headers}.
	 *
	 * @param source The {@link Map<String,String> source}.
	 */
	public Headers(Map<String, String> source) {
		super(Header.class, source);
	}
}
