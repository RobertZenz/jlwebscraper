/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * {@link HttpStatus} represents the return state of a single HTTP request.
 */
public class HttpStatus {
	public static final HttpStatus ACCEPTED = new HttpStatus(202, "Accepted");
	public static final HttpStatus ALREADY_REPORTED = new HttpStatus(208, "Already Reported");
	public static final HttpStatus BAD_GATEWAY = new HttpStatus(502, "Bad Gateway");
	public static final HttpStatus BAD_REQUEST = new HttpStatus(400, "Bad Request");
	public static final HttpStatus CONFLICT = new HttpStatus(409, "Conflict");
	public static final HttpStatus CONTINUE = new HttpStatus(100, "Continue");
	public static final HttpStatus CREATED = new HttpStatus(201, "Created");
	public static final HttpStatus EARLY_HINTS = new HttpStatus(103, "Early Hints");
	public static final HttpStatus EXPECTATION_FAILED = new HttpStatus(417, "Expectation Failed");
	public static final HttpStatus FAILED_DEPENDENCY = new HttpStatus(424, "Failed Dependency");
	public static final HttpStatus FORBIDDEN = new HttpStatus(403, "Forbidden");
	public static final HttpStatus FOUND = new HttpStatus(302, "Found");
	public static final HttpStatus GATEWAY_TIMEOUT = new HttpStatus(504, "Gateway Timeout");
	public static final HttpStatus GONE = new HttpStatus(410, "Gone");
	public static final HttpStatus HTTP_VERSION_NOT_SUPPORTED = new HttpStatus(505, "HTTP Version Not Supported");
	public static final HttpStatus IM_A_TEAPOT = new HttpStatus(418, "I'm a teapot");
	public static final HttpStatus IM_USED = new HttpStatus(209, "IM Used");
	public static final HttpStatus INSUFFICIENT_STORAGE = new HttpStatus(507, "Insufficient Storage");
	public static final HttpStatus INTERNAL_SERVER_ERROR = new HttpStatus(500, "Internal Server Error");
	public static final HttpStatus LENGTH_REQUIRED = new HttpStatus(411, "Length Required");
	public static final HttpStatus LOCKED = new HttpStatus(423, "Locked");
	public static final HttpStatus LOOP_DETECTED = new HttpStatus(508, "Loop Detected");
	public static final HttpStatus METHOD_NOT_ALLOWED = new HttpStatus(405, "Method Not Allowed");
	public static final HttpStatus MISDIRECTED_REQUEST = new HttpStatus(421, "Misdirected Request");
	public static final HttpStatus MOVED_PERMANENTLY = new HttpStatus(301, "Moved permanently");
	public static final HttpStatus MULTI_STATUS = new HttpStatus(207, "Multi-Status");
	public static final HttpStatus MULTIPLE_CHOICES = new HttpStatus(300, "Multiple Choices");
	public static final HttpStatus NETWORK_AUTHENTICATION_REQUIRED = new HttpStatus(511, "Network Authentication Required");
	public static final HttpStatus NO_CONTENT = new HttpStatus(204, "No Content");
	public static final HttpStatus NON_AUTHORITATIVE_INFORMATION = new HttpStatus(203, "Non-Authoritative Information");
	public static final HttpStatus NOT_ACCEPTABLE = new HttpStatus(406, "Not Acceptable");
	public static final HttpStatus NOT_EXTENDED = new HttpStatus(510, "Not Extended");
	public static final HttpStatus NOT_FOUND = new HttpStatus(404, "Not Found");
	public static final HttpStatus NOT_IMPLEMENTED = new HttpStatus(501, "Not Implemented");
	public static final HttpStatus NOT_MODIFIED = new HttpStatus(304, "Not Modified");
	public static final HttpStatus OK = new HttpStatus(200, "OK");
	public static final HttpStatus PARTIAL_CONTENT = new HttpStatus(206, "Partial Content");
	public static final HttpStatus PAYLOAD_TOO_LARGE = new HttpStatus(413, "Payload Too Large");
	public static final HttpStatus PAYMENT_REQUIRED = new HttpStatus(402, "Payment Required");
	public static final HttpStatus PERMANENT_REDIRECT = new HttpStatus(308, "Permanent Redirect");
	public static final HttpStatus PRECONDITION_FAILED = new HttpStatus(412, "Precondition Failed");
	public static final HttpStatus PRECONDITION_REQUIRED = new HttpStatus(428, "Precondition Required");
	public static final HttpStatus PROCESSING = new HttpStatus(102, "Processing");
	public static final HttpStatus PROXY_AUTHENTICATION_REQUIRED = new HttpStatus(407, "Proxy Authentication Required");
	public static final HttpStatus RANGE_NOT_SATISFIABLE = new HttpStatus(416, "Range Not Satisfiable");
	public static final HttpStatus REQUEST_HEADER_FIELDS_TOO_LARGE = new HttpStatus(431, "Request Header Fields Too Large");
	public static final HttpStatus REQUEST_TIMEOUT = new HttpStatus(408, "Request Timeout");
	public static final HttpStatus RESET_CONTENT = new HttpStatus(205, "Reset Content");
	public static final HttpStatus SEE_OTHER = new HttpStatus(303, "See Other");
	public static final HttpStatus SERVICE_UNAVAILABLE = new HttpStatus(503, "Service Unavailable");
	public static final HttpStatus SWITCH_PROXY = new HttpStatus(306, "Switch Proxy");
	public static final HttpStatus SWITCHING_PROTOCOLS = new HttpStatus(101, "Switching Protocols");
	public static final HttpStatus TEMPORARY_REDIRECT = new HttpStatus(307, "Temporary Redirect");
	public static final HttpStatus TOO_MANY_REQUESTS = new HttpStatus(429, "Too Many Requests");
	public static final HttpStatus UNAUTHORIZED = new HttpStatus(401, "Unauthorized");
	public static final HttpStatus UNAVAILABLE_FOR_LEGAL_REASONS = new HttpStatus(451, "Unavailable For Legal Reasons");
	public static final HttpStatus UNPROCESSABLE_ENTITY = new HttpStatus(422, "Unprocessable Entity");
	public static final HttpStatus UNSUPPORTED_MEDIA_TYPE = new HttpStatus(415, "Unsupported Media Type");
	public static final HttpStatus UPGRADE_REQUIRED = new HttpStatus(426, "Upgrade Required");
	public static final HttpStatus URI_TOO_LONG = new HttpStatus(414, "URI Too Long");
	public static final HttpStatus USE_PROXY = new HttpStatus(305, "Use Proxy");
	public static final HttpStatus VARIANT_ALSO_NEGOTIATES = new HttpStatus(506, "Variant Also Negotiates");
	
	protected int code = -1;
	protected String message = null;
	protected HttpStatusType type = HttpStatusType.UNKOWN;
	
	public HttpStatus(int code, String message) {
		super();
		
		this.code = code;
		this.message = message;
		
		for (HttpStatusType httpStatusType : HttpStatusType.ALL) {
			if ((code / 100) == (httpStatusType.getGroup() / 100)) {
				this.type = httpStatusType;
			}
		}
	}
	
	/**
	 * Gets or creates a {@link HttpStatus}.
	 * 
	 * @param code The code which identifies the status.
	 * @param message The optional message if the status has to be created.
	 * @return The static instance or a newly created one.
	 */
	public static final HttpStatus get(int code, String message) {
		for (Field field : HttpStatus.class.getFields()) {
			if (Modifier.isPublic(field.getModifiers())
					&& Modifier.isStatic(field.getModifiers())
					&& HttpStatus.class.isAssignableFrom(field.getType())) {
				HttpStatus httpStatus = null;
				
				try {
					httpStatus = (HttpStatus)field.get(null);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// Ignore any problems.
				}
				
				if (httpStatus != null && httpStatus.getCode() == code) {
					return httpStatus;
				}
			}
		}
		
		return new HttpStatus(code, message);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		HttpStatus other = (HttpStatus)obj;
		
		if (code != other.code) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return The code.
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * Gets the message.
	 *
	 * @return The message.
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Gets the {@link HttpStatusType type}.
	 *
	 * @return The {@link HttpStatusType}.
	 */
	public HttpStatusType getType() {
		return type;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + code;
		
		return result;
	}
	
	/**
	 * Gets whether this {@link HttpStatus} represents a client error.
	 * 
	 * @return {@code true} if this {@link HttpStatus} represents a client
	 *         error.
	 */
	public boolean isClientError() {
		return getType() == HttpStatusType.CLIENT_ERROR;
	}
	
	/**
	 * Gets whether this {@link HttpStatus} represents an information response.
	 * 
	 * @return {@code true} if this {@link HttpStatus} represents an information
	 *         response.
	 */
	public boolean isInformation() {
		return getType() == HttpStatusType.INFORMATION;
	}
	
	/**
	 * Gets whether this {@link HttpStatus} represents a redirection.
	 * 
	 * @return {@code true} if this {@link HttpStatus} represents a redirection.
	 */
	public boolean isRedirection() {
		return getType() == HttpStatusType.REDIRECTION;
	}
	
	/**
	 * Gets whether this {@link HttpStatus} represents a server error.
	 * 
	 * @return {@code true} if this {@link HttpStatus} represents a server
	 *         error.
	 */
	public boolean isServerError() {
		return getType() == HttpStatusType.SERVER_ERROR;
	}
	
	/**
	 * Gets whether this {@link HttpStatus} represents a success.
	 * 
	 * @return {@code true} if this {@link HttpStatus} represents a success.
	 */
	public boolean isSuccess() {
		return getType() == HttpStatusType.SUCCESS;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return code + " " + message;
	}
}
