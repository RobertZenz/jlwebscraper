/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication;

import java.util.Map;

import org.bonsaimind.jlwebscraper.web.communication.base.AbstractData;

/**
 * A POST data holder.
 */
public class PostData extends AbstractData<PostData, PostDataItem> {
	/**
	 * Creates a new instance of {@link PostData}.
	 */
	public PostData() {
		super(PostDataItem.class);
	}
	
	/**
	 * Creates a new instance of {@link PostData}.
	 *
	 * @param source The {@link Map<String,String> source}.
	 */
	public PostData(Map<String, String> source) {
		super(PostDataItem.class, source);
	}
	
	/**
	 * Creates a new instance of {@link PostData}.
	 *
	 * @param source The {@link PostData source}.
	 */
	public PostData(PostData source) {
		super(PostDataItem.class, source);
	}
}
