/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication;

import java.io.IOException;
import java.util.Map.Entry;

import org.bonsaimind.jlwebscraper.web.html.Document;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.Jsoner;

/**
 * The {@link Request} class is the main client class which allows to place
 * requests against a server and process the response.
 * <p>
 * There is no dedicated class to represent a response, instead the properties
 * of the latest response can be accessed through this class.
 * <p>
 * This class is not thread-safe.
 */
public class Request {
	/** The default {@link RedirectBehavior}. */
	public static final RedirectBehavior DEFAULT_REDIRECT_BEHAVIOR = RedirectBehavior.FOLLOW;
	/** A variable representing an infinite timeout. */
	public static final int INFINITE_TIMEOUT = 0;
	
	/** The {@link CookieJar} which will be used. */
	protected CookieJar cookieJar = new CookieJar();
	/** The {@link RedirectBehavior} to use. */
	protected RedirectBehavior redirectBehavior = RedirectBehavior.FOLLOW;
	/** The {@link Headers} of the request. */
	protected Headers requestHeaders = new Headers();
	/** The latest {@link Response}, if any. */
	protected Response response = null;
	/** The responded {@link Document}, if any. */
	protected Document responseDocument = null;
	/** The responded {@link Headers}, if any. */
	protected Headers responseHeaders = null;
	/** The responded JSON object, if any. */
	protected Object responseJson = null;
	/** The responded {@link HttpStatus}. */
	protected HttpStatus responseStatus = null;
	/** Whether an exception should be thrown if there is an error (400/500). */
	protected boolean throwOnError = false;
	/** The timeout for a request. */
	protected int timeoutInMilliseconds = 15000;
	/** The base URL to be used. */
	protected String url = null;
	/** The user-agent to send. */
	protected String userAgent = "";
	
	/**
	 * Creates a new instance of {@link Request}.
	 *
	 * @param url The {@link String base-URL} which will be used for every
	 *        request.
	 */
	public Request(String url) {
		super();
		
		this.url = url;
	}
	
	/**
	 * Perform a DELETE request.
	 * 
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doDelete() throws RequestException {
		return execute(null, Method.DELETE, null);
	}
	
	/**
	 * Perform a DELETE request with a path.
	 * 
	 * @param path The path to add to the current base URL.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doDelete(String path) throws RequestException {
		return execute(path, Method.DELETE, null);
	}
	
	/**
	 * Perform a GET request.
	 * 
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doGet() throws RequestException {
		return execute(null, Method.GET, null);
	}
	
	/**
	 * Perform a GET request with a path.
	 * 
	 * @param path The path to add to the current base URL.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doGet(String path) throws RequestException {
		return execute(path, Method.GET, null);
	}
	
	/**
	 * Perform a HEAD request.
	 * 
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doHead() throws RequestException {
		return execute(null, Method.HEAD, null);
	}
	
	/**
	 * Perform a HEAD request with a path.
	 * 
	 * @param path The path to add to the current base URL.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doHead(String path) throws RequestException {
		return execute(path, Method.HEAD, null);
	}
	
	/**
	 * Perform an OPTION request.
	 * 
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doOptions() throws RequestException {
		return execute(null, Method.OPTIONS, null);
	}
	
	/**
	 * Perform an OPTIONS request with a path.
	 * 
	 * @param path The path to add to the current base URL.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doOptions(String path) throws RequestException {
		return execute(path, Method.OPTIONS, null);
	}
	
	/**
	 * Perform a PATCH request.
	 * 
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPatch(PostData data) throws RequestException {
		return execute(null, Method.PATCH, data);
	}
	
	/**
	 * Perform a PATCH request.
	 * 
	 * @param path The path to add to the current base URL.
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPatch(String path, PostData data) throws RequestException {
		return execute(path, Method.PATCH, data);
	}
	
	/**
	 * Perform a POST request.
	 * 
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPost(PostData data) throws RequestException {
		return execute(null, Method.POST, data);
	}
	
	/**
	 * Perform a POST request.
	 * 
	 * @param path The path to add to the current base URL.
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPost(String path, PostData data) throws RequestException {
		return execute(path, Method.POST, data);
	}
	
	/**
	 * Perform a PUT request.
	 * 
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPut(PostData data) throws RequestException {
		return execute(null, Method.PUT, data);
	}
	
	/**
	 * Perform a PUT request.
	 * 
	 * @param path The path to add to the current base URL.
	 * @param data The {@link PostData data} to send.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doPut(String path, PostData data) throws RequestException {
		return execute(path, Method.PUT, data);
	}
	
	/**
	 * Perform a TRACE request.
	 * 
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doTrace() throws RequestException {
		return execute(null, Method.TRACE, null);
	}
	
	/**
	 * Perform a TRACE request with a path.
	 * 
	 * @param path The path to add to the current base URL.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	public HttpStatus doTrace(String path) throws RequestException {
		return execute(path, Method.TRACE, null);
	}
	
	/**
	 * Gets the {@link CookieJar}.
	 *
	 * @return The {@link CookieJar}, never {@code null}.
	 */
	public CookieJar getCookieJar() {
		return cookieJar;
	}
	
	/**
	 * Gets the {@link RedirectBehavior}.
	 *
	 * @return The {@link RedirectBehavior}.
	 */
	public RedirectBehavior getRedirectBehavior() {
		return redirectBehavior;
	}
	
	/**
	 * Gets the {@link Headers} of the current request.
	 * 
	 * @return The {@link Headers} of the current request, cannot be
	 *         {@code null}.
	 */
	public Headers getRequestHeaders() {
		return requestHeaders;
	}
	
	/**
	 * Gets the (raw) body of the last response.
	 * 
	 * @return The (raw) body of the last response.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception.
	 */
	public String getResponseBody() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		return response.body();
	}
	
	/**
	 * Gets the content type of the last response.
	 * 
	 * @return The content type of the last response.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception.
	 */
	public String getResponseContentType() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		return response.contentType();
	}
	
	/**
	 * Gets the last response as {@link Document}.
	 * 
	 * @return The last response as {@link Document}.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception. Also if the
	 *         response could not be parsed as HTML document.
	 */
	public Document getResponseDocument() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		if (responseDocument == null) {
			try {
				responseDocument = new Document(response.parse());
			} catch (IOException e) {
				throw new IllegalStateException("Response could not be parsed.", e);
			}
		}
		
		return responseDocument;
	}
	
	/**
	 * Gets the {@link Headers} of the last response.
	 * 
	 * @return The {@link Headers} of the last response.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception.
	 */
	public Headers getResponseHeaders() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		if (responseHeaders == null) {
			responseHeaders = new Headers(response.headers());
		}
		
		return responseHeaders;
	}
	
	/**
	 * Gets the last response as JSON object.
	 * 
	 * @return The last response as JSON object.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception. Also if the
	 *         response could not be parsed as JSON.
	 */
	public Object getResponseJson() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		if (responseJson == null) {
			try {
				return Jsoner.deserialize(response.body());
			} catch (JsonException e) {
				throw new IllegalStateException("Response could not be parsed.", e);
			}
		}
		
		return responseJson;
	}
	
	/**
	 * Gets the {@link HttpStatus} of the last response.
	 * 
	 * @return The {@link HttpStatus} of the last response.
	 * @throws IllegalStateException If there is no response because no request
	 *         was made or the last one failed with an exception.
	 */
	public HttpStatus getResponseStatus() {
		if (response == null) {
			throw new IllegalStateException("Response is not set, either no request was made or the last request failed with an exception.");
		}
		
		return responseStatus;
	}
	
	/**
	 * Gets the current timeout for the request.
	 * 
	 * @return The current timeout for the request.
	 */
	public int getTimeout() {
		return timeoutInMilliseconds;
	}
	
	/**
	 * Gets the URL of the current request.
	 * 
	 * @return The URL of the current request.
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Gets the user agent of the current request.
	 *
	 * @return The user agent of the current request.
	 */
	public String getUserAgent() {
		return userAgent;
	}
	
	/**
	 * Gets whether a {@link RequestException} should be thrown if there is a
	 * 400/500 response.
	 * 
	 * @return {@code true} if a {@link RequestException} should be thrown if
	 *         there is a 400/500 response.
	 */
	public boolean isThrowOnError() {
		return throwOnError;
	}
	
	/**
	 * Sets the {@link CookieJar} to be used.
	 * 
	 * @param cookieJar The {@link CookieJar} to use. If it is {@code null}, a
	 *        new instance will be set instead.
	 * @return This instance.
	 */
	public Request setCookieJar(CookieJar cookieJar) {
		if (cookieJar == null) {
			this.cookieJar = new CookieJar();
		} else {
			this.cookieJar = cookieJar;
		}
		
		return this;
	}
	
	/**
	 * Sets the {@link RedirectBehavior} to be used.
	 * 
	 * @param redirectBehavior The {@link RedirectBehavior} to use. If it is
	 *        {@code null}, the default ({@link #DEFAULT_REDIRECT_BEHAVIOR})
	 *        will be used.
	 * @return This instance.
	 */
	public Request setRedirectBehavior(RedirectBehavior redirectBehavior) {
		if (redirectBehavior == null) {
			this.redirectBehavior = DEFAULT_REDIRECT_BEHAVIOR;
		} else {
			this.redirectBehavior = redirectBehavior;
		}
		
		return this;
	}
	
	/**
	 * Sets the {@link Headers} to be used.
	 * 
	 * @param cookieJar The {@link Headers} to use. If it is {@code null}, a new
	 *        instance will be set instead.
	 * @return This instance.
	 */
	public Request setRequestHeaders(Headers requestHeaders) {
		if (requestHeaders == null) {
			this.requestHeaders = new Headers();
		} else {
			this.requestHeaders = requestHeaders;
		}
		
		return this;
	}
	
	/**
	 * Sets whether a {@link RequestException} should be thrown if there is a
	 * 400/500 response.
	 * 
	 * @param throwOnError {@code true} if a {@link RequestException} should be
	 *        thrown if there is a 400/500 response.
	 */
	public void setThrowOnError(boolean throwOnError) {
		this.throwOnError = throwOnError;
	}
	
	/**
	 * Sets the timeout to be used, in milliseconds.
	 * 
	 * @param timeoutInMilliseconds The timeout to use, in milliseconds. Can be
	 *        {@code 0} or negative for no timeout.
	 * @return This instance.
	 */
	public Request setTimeout(int timeoutInMilliseconds) {
		if (timeoutInMilliseconds < 0) {
			this.timeoutInMilliseconds = INFINITE_TIMEOUT;
		} else {
			this.timeoutInMilliseconds = timeoutInMilliseconds;
		}
		
		return this;
	}
	
	/**
	 * Sets the user-agent to be used.
	 * 
	 * @param cookieJar The user-agent to use. If it is {@code null}, an empty
	 *        one will be used.
	 * @return This instance.
	 */
	public Request setUserAgent(String userAgent) {
		if (userAgent == null) {
			this.userAgent = "";
		} else {
			this.userAgent = userAgent;
		}
		
		return this;
	}
	
	/**
	 * Create a new {@link Connection} for the given target.
	 * 
	 * @param target The URL.
	 * @return A new {@link Connection}.
	 */
	protected Connection createConnection(String target, Method method, PostData postData) {
		Connection connection = Jsoup.connect(target)
				.followRedirects(redirectBehavior == RedirectBehavior.FOLLOW)
				.ignoreHttpErrors(!throwOnError)
				.ignoreContentType(true)
				.timeout(timeoutInMilliseconds)
				.userAgent(userAgent);
		
		for (Header header : requestHeaders.getAll()) {
			connection.header(header.getKey(), header.getValue());
		}
		
		connection.request()
				.method(method);
		
		if (postData != null) {
			for (PostDataItem postDataItem : postData.getAll()) {
				connection.data(postDataItem.getKey(), postDataItem.getValue());
			}
		}
		
		for (Cookie cookie : cookieJar.getAll()) {
			connection.cookie(cookie.getKey(), cookie.getValue());
		}
		
		return connection;
	}
	
	/**
	 * Execute the given operation.
	 * 
	 * @param path The (relative) URL to which to connect, can be {@code null}
	 *        or empty.
	 * @param method The {@link Method} to use.
	 * @param postData The data to send, can be {@code null} if there is none.
	 * @return The {@link HttpStatus} of the response.
	 * @throws RequestException If the request fails.
	 */
	protected HttpStatus execute(String path, Method method, PostData postData) throws RequestException {
		response = null;
		responseDocument = null;
		responseHeaders = null;
		responseJson = null;
		responseStatus = null;
		
		String target = url;
		
		if (path != null && !path.isEmpty()) {
			target = url + path;
		}
		
		Connection connection = createConnection(target, method, postData);
		
		try {
			do {
				response = connection.execute();
				responseStatus = HttpStatus.get(response.statusCode(), response.statusMessage());
			} while (responseStatus.isRedirection() && redirectBehavior == RedirectBehavior.FOLLOW);
		} catch (IOException e) {
			throw new RequestException(e);
		}
		
		for (Entry<String, String> cookie : response.cookies().entrySet()) {
			cookieJar.add(new Cookie(cookie.getKey(), cookie.getValue()));
		}
		
		return responseStatus;
	}
}
