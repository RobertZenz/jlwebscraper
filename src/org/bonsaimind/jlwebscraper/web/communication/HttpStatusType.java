/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.web.communication;

import java.util.EnumSet;

/**
 * {@link HttpStatusType} represents the type of HTTP response.
 */
public enum HttpStatusType {
	/** The request was malformed, fault of the client. */
	CLIENT_ERROR(400),
	
	/** Response only provided information. */
	INFORMATION(100),
	
	/** Response suggests to reroute the request. */
	REDIRECTION(300),
	
	/** The server could not process the request. */
	SERVER_ERROR(500),
	
	/** Request completed successfully. */
	SUCCESS(200),
	
	/** State is unknown. */
	UNKOWN(0);
	
	/** A cached instance of all values. */
	public static final EnumSet<HttpStatusType> ALL = EnumSet.allOf(HttpStatusType.class);
	
	/** The group of the status code. */
	protected int group = -1;
	
	/**
	 * Creates a new instance of {@link HttpStatusType}.
	 *
	 * @param group The {@link int}.
	 */
	private HttpStatusType(int group) {
		this.group = group;
	}
	
	/**
	 * Gets the group.
	 *
	 * @return The group
	 */
	public int getGroup() {
		return group;
	}
}
