/*
 * Copyright 2019, Robert 'Bobby' Zenz
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA
 */

package org.bonsaimind.jlwebscraper.lua;

import java.util.Map.Entry;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;

public final class JsonLuaCoerce {
	private JsonLuaCoerce() {
	}
	
	@SuppressWarnings("unchecked")
	public static final LuaValue coerce(Object object) {
		if (object instanceof JsonObject) {
			JsonObject jsonObject = (JsonObject)object;
			LuaTable luaTable = new LuaTable();
			
			for (Object entry : jsonObject.entrySet()) {
				luaTable.set(
						((Entry<String, Object>)entry).getKey(),
						coerce(((Entry<String, Object>)entry).getValue()));
			}
			
			return luaTable;
		} else if (object instanceof JsonArray) {
			JsonArray jsonArray = (JsonArray)object;
			LuaTable luaTable = new LuaTable();
			
			for (Object item : jsonArray) {
				luaTable.set(luaTable.length() + 1, coerce(item));
			}
			
			return luaTable;
		} else if (object instanceof String) {
			// If it is a String, try parse it as JSON. if that yields another
			// String or fails, we return it as is.
			try {
				Object parsedJson = Jsoner.deserialize((String)object);
				
				if (!(parsedJson instanceof String)) {
					return coerce(parsedJson);
				}
			} catch (JsonException e) {
				// Not a valid JSON object, return it as is.
			}
			
			return CoerceJavaToLua.coerce(object);
		} else {
			return CoerceJavaToLua.coerce(object);
		}
	}
}
